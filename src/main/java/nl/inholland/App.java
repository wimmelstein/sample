package nl.inholland;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Function<Integer, Integer> multiply = null;
        System.out.println("Options:\n1) multiply 10 by 2\n2) multiply 10 by 4\nYour option: ");
        Scanner scanner = new Scanner(System.in);
        String option = scanner.nextLine();
        switch (option) {
            case "1": multiply = i -> i * 2; break;
            case "2": multiply = i -> i * 4; break;
            default: System.exit(0);
        }
        System.out.println(calculate(10, multiply));

    }

    public static Integer calculate(int a, Function<Integer, Integer> m) {
        return m.apply(a);
    }
}
