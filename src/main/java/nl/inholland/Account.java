package nl.inholland;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component("account")
@Getter
@Setter
@ToString
public class Account {

    @Value("INHO")
    private String accountNumber;



}
