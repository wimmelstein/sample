package nl.inholland;

@FunctionalInterface
public interface Swim {

    void stroke();
    default void crawl() {
        System.out.println("doing the crawl");
    }
}
